import pygame
import pygame.gfxdraw


class Region:
    def __init__(self, x, y, name, population):
        self.x, self.y = x, y
        self.name = name
        self.population = population
        self.nbinfected = 0
        self.isinfected = False

    def updateinfected(self):
        self.nbinfected = 0

    def dead(self):
        self.population -= 1

    def infect(self):
        self.isinfected = True

    def draw(self, screen):
        if self.isinfected:
            pygame.gfxdraw.filled_circle(screen, self.x, self.y, 10, (255, 0, 0, 200))
            pygame.gfxdraw.circle(screen, self.x, self.y, 10, (250, 250, 250, 200))
            pygame.gfxdraw.filled_trigon(screen, self.x+5, self.y-3, self.x-5, self.y-3, self.x, self.y+3, (250, 250, 250, 200))
