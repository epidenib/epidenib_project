import pygame
# import sys


class Button:
    def __init__(self, x, y):
        self.width = 150
        self.height = 70
        self.pos = [x, y]

        self.color = (233, 0, 234)
        self.color_light = (170, 170, 170)
        self.color_i = self.color_light
        self.color_dark = (100, 100, 100)
        self.color_border = self.color_dark
        self.isclicked = False

        # smallfont = pygame.font.SysFont('Corbel', 35)
        # text = smallfont.render('quit', True, self.color)

    def clicked(self, x, y):
        if not self.isclicked:
            if self.pos[0] <= x <= self.pos[0] + self.width and self.pos[1] <= y <= self.pos[1] + self.height:
                if self.color_i == self.color_dark:
                    self.color_i = self.color_light
                else:
                    self.color_i = self.color_dark
                if self.color_border == self.color_dark:
                    self.color_border = self.color_light
                else:
                    self.color_border = self.color_dark
                self.isclicked = True
                return True
            else:
                self.color_i = self.color_light
                self.color_border = self.color_dark
                return False
        else:
            return False

    def draw_button(self, screen):
        pygame.draw.rect(screen, self.color_i, [self.pos[0], self.pos[1], self.width, self.height])
        pygame.draw.rect(screen, self.color_border, [self.pos[0], self.pos[1], self.width, self.height], 3)

        # pygame.draw.rect(screen, color_dark, [width / 2, height / 2, 140, 40])
