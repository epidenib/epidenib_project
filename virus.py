import pygame
import region
from math import *


class Virus:

    def __init__(self, nom):
        self.nom = nom
        self.infectiosite = 10
        self.mortalite = 10
        self.infected_region = []

    def infect(self, reg):
        reg.isinfected = True
        self.infected_region.append(reg)

    # def kill(self,???)
    # augmenter le nombre de morts, utiliser self.mortalite

    # def change infectiosite(self):
    # self.infectiosite += ???

    # def change_mortalite(self):
    # self.mortalite += ???
