import pygame
from window import *
from region import *
from button import *
from action import *
from virus import *
from avion import *
import openpyxl
import random
from pathlib import Path

pygame.init()


class Game:
    def __init__(self):

        data = {}

        xlsx_file = Path('liste_pays.xlsx')
        wb_obj = openpyxl.load_workbook(xlsx_file)
        sheet = wb_obj.active

        for i, row in enumerate(sheet.iter_rows(values_only=True)):
            if i == 0:
                data[row[2]] = []
                data[row[3]] = []
                data[row[4]] = []
                data[row[5]] = []

            else:
                data['Nom'].append(row[2])
                data['Abscisse'].append(row[3])
                data['Ordonnée'].append(row[4])
                data['Population'].append(row[5])

        self.xregion = data['Abscisse']
        self.yregion = data['Ordonnée']
        self.nregion = data['Nom']
        self.pregion = data['Population']
        self.region = []
        self.window = Window()
        self.buttons = {"pause": Button(100, 600), "action": {"action1": Button(300, 300), "action2": Button(500, 300)}}
        self.window.button = self.buttons
        self.actions = [
            Lockdown('Test pour voir si le texte des actions fonctionne', self.buttons['action']['action1']),
            Lockdown('Test pour voir si le texte des actions fonctionne', self.buttons['action']['action2'])]
        self.virus = Virus('Test')
        self.avion = [Avion(self.xregion[2]*self.window.x,self.yregion[2]*self.window.y),
                      Avion(self.xregion[10]*self.window.x,self.yregion[10]*self.window.y),
                      Avion(self.xregion[26]*self.window.x,self.yregion[26]*self.window.y),
                      Avion(self.xregion[40]*self.window.x,self.yregion[40]*self.window.y),
                      Avion(self.xregion[35]*self.window.x,self.yregion[35]*self.window.y),]
        self.window.avion = self.avion
        self.arrivees = []

        for i in range(len(self.avion)):
            self.arrivees.append(random.randint(1,45))

        if len(self.xregion) == len(self.xregion) == len(self.nregion) == len(self.pregion):
            for i in range(len(self.yregion)):
                self.region.append(
                    Region(int(self.xregion[i] * self.window.x), int(self.yregion[i] * self.window.y), self.nregion[i],
                           self.pregion[i]))
            self.window.region = self.region
        else:
            quit()

        pygame.display.set_caption("EPIDENIB")

    def run(self):
        running = True
        pause = "none"
        test = True
        act = 0
        self.virus.infect(self.region[6])
        pause = "none"
        print(self.window.x, self.window.y)
        while running:
            self.window.draw_window()
            if pause == "none":
                self.window.draw_map()
                for event in pygame.event.get():
                    if event.type == pygame.KEYDOWN:
                        if event.key == pygame.K_TAB:
                            pause = "pauseInfo"
                        elif event.key == pygame.K_ESCAPE:
                            running = False
                    if event.type == pygame.MOUSEBUTTONUP:
                        pos = pygame.mouse.get_pos()
                        if self.buttons["pause"].clicked(pos[0], pos[1]):
                            pause = "pauseInterface"
                for avion in self.avion:
                    avion.move_avion(self.xregion[43] * self.window.x, self.yregion[43] * self.window.y)
            elif pause == "pauseInterface":
                if test == True:
                    self.window.interface = True
                    for event in pygame.event.get():
                        if event.type == pygame.KEYDOWN:
                            if event.key == pygame.K_ESCAPE:
                                pause = 'none'
                                self.buttons["pause"].isclicked = False
                                self.window.interface = False
                        if event.type == pygame.MOUSEBUTTONUP:
                            pos = pygame.mouse.get_pos()
                            for action in self.actions:
                                if action.button.clicked(pos[0], pos[1]):
                                    act = action
                                    self.window.action = act
                                    test = False
                else:
                    if act != 0:
                        for event in pygame.event.get():
                            if event.type == pygame.KEYDOWN:
                                if event.key == pygame.K_ESCAPE:
                                    act.button.isclicked = False
                                    self.window.action = None
                                    test = True
                                if event.key == pygame.K_RETURN:
                                    self.window.action = None
                                    act.effect()
                                    test = True
            elif pause == "pauseInfo":
                self.window.draw_info = True
                for event in pygame.event.get():
                    if event.type == pygame.KEYDOWN:
                        if event.key == pygame.K_TAB:
                            pause = "none"
                            self.window.draw_info = False
        pygame.quit()
