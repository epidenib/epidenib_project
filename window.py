import pygame
import pygame.gfxdraw


class Window:
    def __init__(self):
        self.map = pygame.image.load("images\carte.png")
        self.globe = pygame.image.load("images\globe_terrestre.png")
        self.screen = pygame.display.set_mode((0, 0), pygame.FULLSCREEN)
        self.x, self.y = self.screen.get_size()
        self.region = []
        self.virus = 0
        self.button = {}
        self.avion = []
        self.action = None
        self.interface = False
        self.draw_info = False

    def draw_window(self):
        self.draw_map()
        self.draw_interface()
        self.draw_action()
        if self.draw_info:
            self.drawInfo()
        pygame.display.flip()

    def draw_map(self):
        self.screen.blit(pygame.transform.scale(self.map, (self.x, self.y)), (0, 0))
        for reg in self.region:
            reg.draw(self.screen)
        if self.button:
            self.button["pause"].draw_button(self.screen)
        for avion in self.avion:
            avion.draw_avion(self.screen)

    def draw_interface(self):
        if self.interface:
            pygame.gfxdraw.box(self.screen, [200, 200, 500, 600], (99, 110, 128, 255))
            for button in self.button["action"].values():
                button.draw_button(self.screen)

    def draw_action(self):
        if self.action:
            self.action.draw_action(self.screen)

    def drawInfo(self):
        fontTitle = pygame.font.SysFont(None, 42)
        fontSUBTitle = pygame.font.SysFont(None, 37)
        fontSUBTitle.underline = True
        fontLegend = pygame.font.SysFont(None, 30, bold = True)
        fontParagraph = pygame.font.SysFont(None, 30)

        backgroundRect = pygame.Rect(0.125*self.x, 0.125*self.y, 0.75*self.x, 0.75*self.y)
        pygame.gfxdraw.box(self.screen, backgroundRect, (99, 110, 128, 200))

        legendRectSain = pygame.Rect(backgroundRect[0] + 0.16 * self.x, backgroundRect[1] + 0.68 * self.y,
                                     backgroundRect[2] - 0.7 * self.x, backgroundRect[3] - 0.71 * self.y)
        pygame.draw.rect(self.screen, (44, 165, 245), legendRectSain)

        legendRectInfecte = pygame.Rect(backgroundRect[0] + 0.405 * self.x, backgroundRect[1] + 0.68 * self.y,
                                     backgroundRect[2] - 0.702 * self.x, backgroundRect[3] - 0.71 * self.y)
        pygame.draw.rect(self.screen, (199, 48, 106), legendRectInfecte)

        legendRectMort = pygame.Rect(backgroundRect[0] + 0.65 * self.x, backgroundRect[1] + 0.68 * self.y,
                                        backgroundRect[2] - 0.7 * self.x, backgroundRect[3] - 0.71 * self.y)
        pygame.draw.rect(self.screen, (105, 27, 148), legendRectMort)

        infos_regionales = fontTitle.render("Informations régionales", True, (255, 255, 255))
        sain = fontLegend.render("Population saine", True, (255, 255, 255))
        infecte = fontLegend.render("Population infectée", True, (255, 255, 255))
        mort = fontLegend.render("Population morte", True, (255, 255, 255))

        NA = fontSUBTitle.render("Amérique du Nord", True, (255, 255, 255))
        SA = fontSUBTitle.render("Amérique du Sud", True, (255, 255, 255))
        EU = fontSUBTitle.render("Europe", True, (255, 255, 255))

        self.screen.blit(infos_regionales, (backgroundRect[0]+0.075*self.x,backgroundRect[1]+0.04375*self.y))
        self.screen.blit(sain, (backgroundRect[0] + 0.025 * self.x, backgroundRect[3] + 0.065 * self.y))
        self.screen.blit(infecte, (backgroundRect[0] + 0.25 * self.x, backgroundRect[3] + 0.065 * self.y))
        self.screen.blit(mort, (backgroundRect[0] + 0.51 * self.x, backgroundRect[3] + 0.065 * self.y))

        self.screen.blit(pygame.transform.scale(self.globe, (75, 75)), (backgroundRect[0]+0.0125*self.x,backgroundRect[1]+0.0125*self.y))

        self.screen.blit(NA, (backgroundRect[0] + 0.04 * self.x, backgroundRect[1] + 0.13 * self.y))
        self.screen.blit(SA, (backgroundRect[0] + 0.04 * self.x, backgroundRect[1] + 0.374 * self.y))
        self.screen.blit(EU, (backgroundRect[0] + 0.385 * self.x, backgroundRect[1] + 0.13 * self.y))

        for i in range(5):
            nom = fontParagraph.render(self.region[i].name, True, (255, 255, 255))
            population = fontParagraph.render(str(self.region[i].population), True, (255, 255, 255))
            self.screen.blit(nom, (backgroundRect[0] + 0.04 * self.x, backgroundRect[1] + (i+5)*0.035 * self.y))
            self.screen.blit(population, (backgroundRect[0] + 0.16 * self.x, backgroundRect[1] + (i + 5) * 0.035 * self.y))

        for i in range(6, 12):
            nom = fontParagraph.render(self.region[i].name, True, (255, 255, 255))
            population = fontParagraph.render(str(self.region[i].population), True, (255, 255, 255))
            self.screen.blit(nom, (backgroundRect[0] + 0.04 * self.x, backgroundRect[1] + (i+6)*0.035 * self.y))
            self.screen.blit(population, (backgroundRect[0] + 0.16 * self.x, backgroundRect[1] + (i+6)*0.035 * self.y))

        for i in range(12,25):
            nom = fontParagraph.render(self.region[i].name, True, (255, 255, 255))
            population = fontParagraph.render(str(self.region[i].population), True, (255, 255, 255))
            self.screen.blit(nom, (backgroundRect[0] + 0.385 * self.x, backgroundRect[1] + (i-7) * 0.035 * self.y))
            self.screen.blit(population, (backgroundRect[0] + 0.545 * self.x, backgroundRect[1] + (i-7) * 0.035 * self.y))