from math import *
import pygame


class Avion:

    def __init__(self, avion_x, avion_y):
        self.avion = pygame.transform.scale(pygame.image.load('images/avion.png'), (60, 60))
        self.avion_x = avion_x - self.avion.get_width()/2
        self.avion_y = avion_y - self.avion.get_height()/2
        self.depart = [float(avion_x - self.avion.get_width() / 2), float(avion_y - self.avion.get_height() / 2)]
        self.direction = False

    def draw_avion(self, screen):
        screen.blit(self.avion, (self.avion_x, self.avion_y))

    def move_avion(self, x2, y2):
        # Definir le dx et le dy du deplacement de l'avion
        dx = float(x2 - self.avion.get_width() / 2) - self.depart[0]
        dy = float(y2 - self.avion.get_height() / 2) - self.depart[1]
        # Faire pointer l'avion vers sa destination
        if self.direction == False:
            if dx > 0 and dy > 0:
                self.avion = pygame.transform.rotate(self.avion, 130 + degrees(atan(dx / dy)))
            elif dx > 0 and dy < 0:
                self.avion = pygame.transform.rotate(self.avion, 195 - degrees(atan(dx / dy)))
            elif dx < 0 and dy < 0:
                self.avion = pygame.transform.rotate(self.avion, degrees(atan(dx / dy)) - 45)
            elif dx < 0 and dy > 0:
                self.avion = pygame.transform.rotate(self.avion, degrees(atan(dx / dy)) + 130)
            self.direction = True
        # Faire voler l'avion jusqu'à sa destination
        if (dx < 0 and self.avion_x > x2 - self.avion.get_width() / 2) or (
                dx > 0 and self.avion_x < x2 - self.avion.get_height() / 2):
            self.avion_x += dx / 700
            self.avion_y += dy / 700