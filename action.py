from abc import ABC, abstractmethod

import pygame


class Action:
    def __init__(self, text_effect, button):
        self.text_effect = text_effect
        self.button = button

    def draw_action(self, screen):
        font = pygame.font.SysFont('Corbel', 42)
        text = font.render(self.text_effect, True, (255, 255, 255))
        pygame.draw.rect(screen, (99, 110, 128), (500, 500, 200, 200))
        screen.blit(text, (500, 500))
        pygame.display.flip()

    @abstractmethod
    def effect(self):
        raise NotImplementedError("Subclass must implement abstract method")


class Lockdown(Action, ABC):
    def __init__(self, text_effect, button):
        super().__init__(text_effect, button)

    def effect(self):
        print('hello')
